# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# setup grid access 
voms-proxy-init -voms atlas 
lsetup ganga 
lsetup rucio
lsetup panda