#!/bin/bash
echo starting batch job initialization
export PATH LD_LIBRARY_PATH PYTHONPATH

function abortJob {
  echo "abort"
  exit 1
}

hostname
pwd
whoami
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
export AtlasSetupSite=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/.config/.asetup.site
export AtlasSetup=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/V02-00-36/AtlasSetup
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh --quiet
export AtlasProject=AnalysisBase
export AtlasVersion=21.2.204
export AtlasBuildStamp=2022-02-17T0354
export AtlasBuildBranch=21.2
export AtlasReleaseType=stable
if [ "${AtlasReleaseType}" == "stable" ]; then
     source ${AtlasSetup}/scripts/asetup.sh ${AtlasProject},${AtlasVersion} || abortJob
else
     source ${AtlasSetup}/scripts/asetup.sh ${AtlasProject},${AtlasBuildBranch},${AtlasBuildStamp} || abortJob
fi

# FILE="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r33-24/HIGG5D2_13TeV/CxAOD_33-24_a/ttbar_nonallhad_PwPy8/group.phys-higgs.mc16_13TeV.410470.CAOD_HIGG5D2.e6337_s3126_r9364_p4308.33-24_CxAOD.root/group.phys-higgs.28324264._000001.CxAOD.root"

# root.exe -b -l -q -e "TFile* file = TFile::Open(\"$FILE\"); if (file == nullptr || file->IsZombie()) exit(1); file->ls(); file->Get(\"CollectionTree\")->Print(); std::cout << \"Success READ file = $FILE\" << std::endl;"

echo "ls -l" 
ls -l 

echo "find /"
find /



cat input.txt 


