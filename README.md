setup 
```
source setup.sh
```

Launch job 
```
./launch.sh
```

Some useful links: 
  * https://panda-wms.readthedocs.io/en/latest/client/prun.html  
  * https://panda-wms.readthedocs.io/en/latest/client/client.html
  `prun index %RNDM:basenumber`