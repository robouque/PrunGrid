TIMESTAMP=$(date "+%Y%m%d_%H%M%S")

OUTPUTDS="user.bouquet.TestGrid_${TIMESTAMP}"
echo "OUTPUTDS=${OUTPUTDS}"

# prun --outDS ${OUTPUTDS} --exec "run.sh" --nJobs 1

# prun --nGBPerJob=2 --exec="run.sh" --outDS ${OUTPUTDS} --inDS=group.phys-higgs.mc16_13TeV.345053.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/,group.phys-higgs.mc16_13TeV.345054.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/ --writeInputToTxt=IN:input.txt --match="group.phys-higgs.28321316._000001.CxAOD.root|group.phys-higgs.28321316._000002.CxAOD.root|group.phys-higgs.28321329._000001.CxAOD.root|group.phys-higgs.28321329._000002.CxAOD.root|group.phys-higgs.28321329._000003.CxAOD.root|group.phys-higgs.28321329._000004.CxAOD.root|group.phys-higgs.28321329._000005.CxAOD.root|group.phys-higgs.28321329._000006.CxAOD.root|group.phys-higgs.28321329._000007.CxAOD.root|group.phys-higgs.28321329._000008.CxAOD.root" --noEmail --expertOnly_skipScout



# prun --nGBPerJob=2 --exec="run.sh" --outDS ${OUTPUTDS} --inDS=group.phys-higgs.mc16_13TeV.345053.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/,group.phys-higgs.mc16_13TeV.345054.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/ --writeInputToTxt=IN:input.txt --match="group.phys-higgs.28321316._000001.CxAOD.root|group.phys-higgs.28321316._000002.CxAOD.root|group.phys-higgs.28321329._000001.CxAOD.root|group.phys-higgs.28321329._000002.CxAOD.root|group.phys-higgs.28321329._000003.CxAOD.root|group.phys-higgs.28321329._000004.CxAOD.root|group.phys-higgs.28321329._000005.CxAOD.root|group.phys-higgs.28321329._000006.CxAOD.root|group.phys-higgs.28321329._000007.CxAOD.root|group.phys-higgs.28321329._000008.CxAOD.root" --expertOnly_skipScout 


# prun --nGBPerJob=2 --exec="run.sh" --outDS ${OUTPUTDS} --inDS=group.phys-higgs.mc16_13TeV.345055.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/ --writeInputToTxt=IN:input.txt --match="\"group.phys-higgs.28321342._000001.CxAOD.root,group.phys-higgs.28321342._000002.CxAOD.root\"" --noEmail --expertOnly_skipScout



rm -f jobcontents.tgz
tar -czvf jobcontents.tgz files

# prun --nGBPerJob=2 --exec="./files/run.sh" --outDS ${OUTPUTDS} --inDS=group.phys-higgs.mc16_13TeV.345055.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/ --writeInputToTxt=IN:input.txt --noEmail --expertOnly_skipScout --inTarBall=jobcontents.tgz --extFile=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/bTaggingCDI/2020-21-13TeV-MC16-CDI_smooth_VHLegacy-07Oct-v02.root 

prun --nGBPerJob=2 --exec="./files/run.sh" --outDS ${OUTPUTDS} --inDS=group.phys-higgs.mc16_13TeV.345055.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/ --writeInputToTxt=IN:input.txt --noEmail --expertOnly_skipScout --inTarBall=jobcontents.tgz --extFile=2020-21-13TeV-MC16-CDI_smooth_VHLegacy-07Oct-v02.root 



# removing 2 files the 000002 and 000003
# prun --nGBPerJob=2 --exec="run.sh" --outDS ${OUTPUTDS} --inDS=group.phys-higgs.mc16_13TeV.345053.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/,group.phys-higgs.mc16_13TeV.345054.CAOD_HIGG5D1.e5706_s3126_r9364_p4310.33-24_CxAOD.root/ --writeInputToTxt=IN:input.txt --match="group.phys-higgs.28321316._000001.CxAOD.root|group.phys-higgs.28321316._000002.CxAOD.root|group.phys-higgs.28321329._000001.CxAOD.root|group.phys-higgs.28321329._000004.CxAOD.root|group.phys-higgs.28321329._000005.CxAOD.root|group.phys-higgs.28321329._000006.CxAOD.root|group.phys-higgs.28321329._000007.CxAOD.root|group.phys-higgs.28321329._000008.CxAOD.root" --noEmail --expertOnly_skipScout